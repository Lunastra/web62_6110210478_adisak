#pragma checksum "D:\Work\NewsReport\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d4c8bc993776ba83c6673e90946760b405901c5a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(NewsReport.Pages.Pages_Index), @"mvc.1.0.razor-page", @"/Pages/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Index.cshtml", typeof(NewsReport.Pages.Pages_Index), null)]
namespace NewsReport.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\Work\NewsReport\Pages\_ViewImports.cshtml"
using NewsReport;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d4c8bc993776ba83c6673e90946760b405901c5a", @"/Pages/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"88c01d45ccfb24904f468bbbfdaedb0a8afbac7d", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "D:\Work\NewsReport\Pages\Index.cshtml"
  
	ViewData["Title"] = "ระบบรายงานข่าว";

#line default
#line hidden
            BeginContext(73, 99, true);
            WriteLiteral("<div class=\"jumbotron\">\r\n\t<center><h2>ระบบรายงานข่าวโดยประชาชนเพื่อประชาชน</h2><center>\r\n</div>\r\n\r\n");
            EndContext();
#line 10 "D:\Work\NewsReport\Pages\Index.cshtml"
 foreach (var item in Model.NewsCategory) {

#line default
#line hidden
            BeginContext(217, 26, true);
            WriteLiteral("\t<div class=\"row\">\r\n\t\t<h3>");
            EndContext();
            BeginContext(244, 38, false);
#line 12 "D:\Work\NewsReport\Pages\Index.cshtml"
       Write(Html.DisplayFor(model=>item.ShortName));

#line default
#line hidden
            EndContext();
            BeginContext(282, 13, true);
            WriteLiteral("</h3>\r\n\t\t<h4>");
            EndContext();
            BeginContext(296, 37, false);
#line 13 "D:\Work\NewsReport\Pages\Index.cshtml"
       Write(Html.DisplayFor(model=>item.FullName));

#line default
#line hidden
            EndContext();
            BeginContext(333, 16, true);
            WriteLiteral("</h4>\r\n\t</div>\r\n");
            EndContext();
            BeginContext(352, 177, true);
            WriteLiteral("\t<div class=\"row\">\r\n\t\t<table class=\"table\">\r\n\t\t\t<thead>\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<th>วันที่เกิดเหตุ</th><th>เน้ือข่าว</th><th>สถานะการยืนยัน</th>\r\n\t\t\t\t</tr>\r\n\t\t\t</thead>\r\n\t\t<tbody>\r\n\t\t\r\n");
            EndContext();
#line 25 "D:\Work\NewsReport\Pages\Index.cshtml"
     foreach (var newsItem in Model.News) {
	if(newsItem.NewsCategoryID == item.NewsCategoryID){

#line default
#line hidden
            BeginContext(625, 21, true);
            WriteLiteral("\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t\t");
            EndContext();
            BeginContext(647, 49, false);
#line 29 "D:\Work\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => newsItem.ReportDate));

#line default
#line hidden
            EndContext();
            BeginContext(696, 25, true);
            WriteLiteral("\r\n\t\t\t</td>\r\n\t\t\t<td>\r\n\t\t\t\t");
            EndContext();
            BeginContext(722, 49, false);
#line 32 "D:\Work\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => newsItem.NewsDetail));

#line default
#line hidden
            EndContext();
            BeginContext(771, 25, true);
            WriteLiteral("\r\n\t\t\t</td>\r\n\t\t\t<td>\r\n\t\t\t\t");
            EndContext();
            BeginContext(797, 49, false);
#line 35 "D:\Work\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => newsItem.NewsStatus));

#line default
#line hidden
            EndContext();
            BeginContext(846, 22, true);
            WriteLiteral("\r\n\t\t\t</td>\r\n\t\t\t</tr>\r\n");
            EndContext();
#line 38 "D:\Work\NewsReport\Pages\Index.cshtml"
			}
		}

#line default
#line hidden
            BeginContext(879, 32, true);
            WriteLiteral("\t\t</tbody>\r\n\t</table>\r\n </div>\r\n");
            EndContext();
#line 43 "D:\Work\NewsReport\Pages\Index.cshtml"
}

#line default
#line hidden
            BeginContext(914, 28, true);
            WriteLiteral("\r\n</tbody>\r\n</table>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
