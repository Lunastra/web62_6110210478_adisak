using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace PharmacyHelper.Models
{
	public class Drug {
		public int DrugID { get; set;}
		public string DrugName { get; set;}
		public int DrugAmout { get; set;}
		
		public string NewsUserId {get; set;}
		public PharmacyOwnerUser postUser {get; set;}
	}
	
	public class PharmacyOwnerUser : IdentityUser{
		public string FirstName { get; set;}
		public string LastName { get; set;}
	}
}
