using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using PharmacyHelper.Data;
using PharmacyHelper.Models;

namespace PharmacyHelper.Pages.DrugListAdmin
{
    public class CreateModel : PageModel
    {
        private readonly PharmacyHelper.Data.PharmacyHelperContext _context;

        public CreateModel(PharmacyHelper.Data.PharmacyHelperContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Drug Drug { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.DrugList.Add(Drug);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}