using System;
using System.ComponentModel.DataAnnotations;

namespace IJune.Models
{
    public class Album
    {
		public int AlbumID { get; set; }
		public string AlbumName { get; set; }
		public string ArtistName { get; set; }
		
		public DateTime ReleaseDate { get; set;}
		public double Price {get; set;}
		
		public string AlbumImage { get; set; }
        public string SampleAudio { get; set; }
        
		public int stock {get; set;}
		public string Review { get; set; }
		public string Label { get; set; }
		public double Rating { get; set; }
	}
	
	public class Order
	{
		public int OrderID { get; set; }
		public string CustomerName { get; set; }
		
		public int AlbumID { get; set; }
		public Album album { get; set; }
		public int Amount { get; set; }
		public string Status { get; set; }
	}
}