using PharmacyHelper.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace PharmacyHelper.Data
{
	public class PharmacyHelperContext : IdentityDbContext<PharmacyOwnerUser>
	{
		public DbSet<Drug> DrugList { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=PharmacyHelper.db");
		}
	}
}