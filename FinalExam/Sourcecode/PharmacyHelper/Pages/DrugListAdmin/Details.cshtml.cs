using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PharmacyHelper.Data;
using PharmacyHelper.Models;

namespace PharmacyHelper.Pages.DrugListAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly PharmacyHelper.Data.PharmacyHelperContext _context;

        public DetailsModel(PharmacyHelper.Data.PharmacyHelperContext context)
        {
            _context = context;
        }

        public Drug Drug { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Drug = await _context.DrugList.FirstOrDefaultAsync(m => m.DrugID == id);

            if (Drug == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
