﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore;
using PharmacyHelper.Data;
using PharmacyHelper.Models;

namespace PharmacyHelper.Pages
{
    public class IndexModel : PageModel
    {
		private readonly PharmacyHelper.Data.PharmacyHelperContext _context;

        public IndexModel(PharmacyHelper.Data.PharmacyHelperContext context)
        {
            _context = context;
        }

        public IList<Drug> Drug { get;set; }

        public async Task OnGetAsync()
        {
            Drug = await _context.DrugList
				.Include(p => p.postUser).ToListAsync();
			Drug = await _context.DrugList.ToListAsync();
        }
    }
}
